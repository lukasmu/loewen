# Changelog

All notable changes to the 🦁 website will be documented in this file.

## 1.0.0 - 2021-03-10

- Initial release

## 1.0.1 - 2022-11-06

- Framework update

## 1.0.2 - 2023-04-20

- Framework update
- Added ticket form (with Stripe integration)

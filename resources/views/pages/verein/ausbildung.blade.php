@extends('layout.main')

@section('title', 'Ausbildung')
@section('image', asset('images/archive/large/pexels-cottonbro-4709811.jpg'))
@section('abstract')
    Die Grundsteine der musikalischen Ausbildung sollten bereits im Kindesalter gelegt werden.
    Im frühen Kindesalter können Noten, Rhythmus und das Zusammenspiel in einer Gruppe leicht spielerisch erlernt werden.
    Daher gibt es im Musikverein Nentershausen verschiedene Ausbildungsstufen, die auf dieser Seite kurz vorgestellt werden.
@endsection

@section('main')
    <div class="container mt-5 mb-4">
        <h2>Musikalische Früherziehung</h2>
        <div class="row">
            <div class="col-12 col-md-4 mb-4">
                <div class="card shadow-sm h-100">
                    <div class="card-header">
                        <b>Anfängerkurs</b>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Für Kinder <b>ab 4 Jahren</b></li>
                        <li class="list-group-item">Unterricht: 1x wöchentlich, 60 Minuten <b>in kleiner Gruppe</b></li>
                        <li class="list-group-item">Dauer: 1 Jahr</li>
                        <li class="list-group-item">Notenschlüssel</li>
                        <li class="list-group-item">Fünftonraum (g1 bis d2)</li>
                        <li class="list-group-item">Notenwerte und Pausen (viertel, halbe und ganze Note sowie Viertelpause)</li>
                        <li class="list-group-item">Dynamik (laut/leise)</li>
                        <li class="list-group-item">Instrumentenfamilien (Streicher, Bläser)</li>
                        <li class="list-group-item">Spielen des Erlernten auf einem <b>Glockenspiel</b></li>
                    </ul>
                    <div class="card-body">
                        <a href="{{ asset('docs/info_mfe.pdf') }}" class="btn btn-primary">Elterninfo herunterladen</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 mb-4">
                <div class="card mb-4 shadow-sm h-100">
                    <div class="card-header">
                        <b>Fortgeschrittenenkurs</b>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Für Kinder <b>ab 5 Jahren</b></li>
                        <li class="list-group-item">Baut auf Anfängerkurs auf</li>
                        <li class="list-group-item">Unterricht: 1x wöchentlich, 60 Minuten <b>in kleiner Gruppe</b></li>
                        <li class="list-group-item">Dauer: 1 Jahr</li>
                        <li class="list-group-item">Erweiterung des Tonraumes (c1 bis e2)</li>
                        <li class="list-group-item">Erweiterung der Notenwerte und Pausen (achtel und punktierte Note, achtel und halbe Pause)</li>
                        <li class="list-group-item">Dynamik (lauter/leiser werden)</li>
                        <li class="list-group-item">Volks- und Kinderlieder, klassische Werke</li>
                        <li class="list-group-item">Tanzformen</li>
                        <li class="list-group-item">Spielen des Erlernten auf einem <b>Glockenspiel</b></li>
                    </ul>
                    <div class="card-body">
                        <a href="{{ url('kontakt') }}" class="btn btn-primary">Fragen?</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 mb-4">
                <div class="card shadow-sm mb-4 h-100">
                    <div class="card-img-wrap card-image-cover">
                        <img src="{{ asset('images/archive/medium/pexels-cottonbro-4709815.jpg') }}" class="card-img" alt="Girl playing the saxophone">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <h2>Instrumentalausbildung</h2>
        <div class="row">
            <div class="d-none d-md-block col-md-3">
                <div class="card shadow-sm h-100">
                    <div class="card-img-wrap card-image-cover">
                        <img src="{{ asset('images/archive/medium/pexels-cottonbro-4786246.jpg') }}" class="card-img" alt="Young man playing the saxophone">
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card shadow-sm h-100">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Für Kinder <b>ab 7 Jahren</b>, Jugendliche und Erwachsene sind natürlich auch herzlich willkommen</li>
                        <li class="list-group-item"><b>Einzelunterricht</b>; 1x wöchentlich, 30-45 Minuten bei qualifizierten Lehrern</li>
                        <li class="list-group-item"><b>Wunschinstrument</b> kann individuell ausgewählt werden</li>
                        <li class="list-group-item">Vorbereitung auf den Eintritt ins Orchester</li>
                        <li class="list-group-item">Erlernen von Noten, Notenwerten, Pausen und Anblastechniken in Theorie und Praxis</li>
                        <li class="list-group-item">Erlernen erster Lieder bis hin zum Üben von Stücken der Orchester</li>
                    </ul>
                    <div class="card-body">
                        <a href="{{ url('kontakt') }}" class="btn btn-primary">Kontakt</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

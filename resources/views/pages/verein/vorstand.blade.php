@extends('layout.main')

@section('title', 'Vorstand')
@section('image', asset('images/archive/large/samuel-ramos-Md8c-amE5ms-unsplash.jpg'))

@section('main')
    <div class="container my-5">
        <table class="table table-striped">
                <thead>
                <tr>

                    <th scope="col">Amt</th>
                    <th scope="col">Name</th>
                    <th scope="col">Kontakt</th>
                </tr>
                </thead>
                <tbody>
                <tr>

                    <td>1. Vorsitzender</td>
                    <td>Bernd Reifenscheidt</td>
                    <td><a class="text-dark" href="mailto:vorsitzender@musikalische-loewen.de" >vorsitzender@musikalische-loewen.de</a> </td>
                </tr>
                <tr>

                    <td>2. Vorsitzender</td>
                    <td>Achim Gläser</td>
                    <td><a class="text-dark" href="mailto:vorsitzender2@musikalische-loewen.de" >vorsitzender2@musikalische-loewen.de</a></td>
                </tr>
                <tr>

                    <td>Kassierer</td>
                    <td>Timo Hofmann</td>
                    <td><a class="text-dark" href="mailto:kassierer@musikalische-loewen.de" >kassierer@musikalische-loewen.de</a></td>
                </tr>
                <tr>

                    <td>Schriftführer</td>
                    <td>Marvin Schulze</td>
                    <td><a class="text-dark" href="mailto:schriftfuehrer@musikalische-loewen.de" >schriftfuehrer@musikalische-loewen.de</a></td>
                </tr>
                <tr>

                    <td>Jugendleiterin</td>
                    <td>Charlin Merfels</td>
                    <td><a class="text-dark" href="mailto:jugendsprecherin@musikalische-loewen.de" >jugendsprecherin@musikalische-loewen.de</a></td>
                </tr>

                <tr>

                    <td>Beisitzer</td>
                    <td>Jan-Phillip Maus</td>
                    <td></td>
                </tr>
                <tr>

                    <td>Beisitzer</td>
                    <td>Julia Born</td>
                    <td></td>
                </tr>
                <tr>

                    <td>Beisitzer</td>
                    <td>Anna-Maria Ortseifen</td>
                    <td></td>
                </tr>
                <tr>

                    <td>Beisitzer</td>
                    <td>Marita Heinelt</td>
                    <td></td>
                </tr>
                <tr>

                    <td>Beisitzer</td>
                    <td>Ingrid Weidenfeller</td>
                    <td></td>
                </tr>
                <tr>

                    <td>Beisitzer</td>
                    <td>Pascal Zängerle</td>
                    <td></td>
                </tr>



                </tbody>
        </table>
    </div>
@endsection
